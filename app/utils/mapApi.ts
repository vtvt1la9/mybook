import { GOOGLE_KEY } from 'app/config/constant-config';
import i18n from 'app/i18n';
import axios from 'axios';

type IGetCurrentPosition = {
  latitude: number;
  longitude: number;
};

type IGetRoute = {
  distance: number;
  duration: number;
};

export const _getRoute = async (
  origin: IGetCurrentPosition,
  destination: IGetCurrentPosition,
): Promise<IGetRoute | any> => {
  try {
    const json = await axios.get(
      `https://maps.googleapis.com/maps/api/directions/json?origin=${origin?.latitude},${origin?.longitude}&destination=${destination?.latitude},${destination?.longitude}&key=${GOOGLE_KEY}&mode=transit`,
    );
    const route = json?.data?.routes[0];
    return {
      distance:
        route?.legs?.reduce((carry: number, curr: any) => {
          return carry + curr?.distance?.value;
        }, 0) / 1000,
      duration:
        route?.legs?.reduce((carry: number, curr: any) => {
          return (
            carry +
            (curr?.duration_in_traffic
              ? curr?.duration_in_traffic?.value
              : curr?.duration?.value)
          );
        }, 0) / 60,
    };
  } catch (error) {
    console.log(`${i18n.t('error')}: ${error}`);
  }
};
