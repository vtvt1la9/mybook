import { createStackNavigator } from "@react-navigation/stack"
import BackBar from "app/components/BackBar";
import BookBar from "app/components/BookBar";
import BookDetailBar from "app/components/BookDetailBar";
import HomeBar from "app/components/HomeBar";
import ProfileBar from "app/components/ProfileBar";
import HomeBookScreen from "app/screens/HomeBook";
import ListBookScreen from "app/screens/ListBooks";
import MyHomeScreen from "app/screens/MyHome";
import ProfileScreen from "app/screens/Profile";
import ReadBookScreen from "app/screens/ReadBook";
import SettingScreen from "app/screens/Setting";
import React from "react";

const Stack = createStackNavigator();
const MyHomeStack = createStackNavigator();

const MyHomeNavigator = () => {

    const [animationTypeForReplace, setAnimationTypeForReplace] = React.useState<
        'pop' | 'push' | undefined
    >('push');

    return (
        <MyHomeStack.Navigator
            initialRouteName="MyHomeScreen"
            headerMode="screen"
        >
            <Stack.Screen
                name="MyHomeScreen"
                component={MyHomeScreen}
                options={{
                    animationTypeForReplace,
                    header: props => <HomeBar />,
                    gestureEnabled: false,
                }}
            />
            <Stack.Screen
                name="ProfileScreen"
                component={ProfileScreen}
                options={{
                    animationTypeForReplace,
                    header: props => <ProfileBar />,
                    gestureEnabled: false,
                }}
            />
            <Stack.Screen
                name="HomeBookScreen"
                component={HomeBookScreen}
                options={{
                    animationTypeForReplace,
                    header: props => <BookBar />,
                    gestureEnabled: false,
                }}
            />
            <Stack.Screen
                name="ReadBookScreen"
                component={ReadBookScreen}
                options={{
                    animationTypeForReplace,
                    header: props => <BookDetailBar />,
                    gestureEnabled: false,
                }}
            />
            <Stack.Screen
                name="ListBookScreen"
                component={ListBookScreen}
                options={{
                    animationTypeForReplace,
                    header: props => <BookDetailBar />,
                    gestureEnabled: false,
                }}
            />
            <Stack.Screen
                name="SettingScreen"
                component={SettingScreen}
                options={{
                    animationTypeForReplace,
                    header: props => <BackBar />,
                    gestureEnabled: false,
                }}
            />
        </MyHomeStack.Navigator >
    )
}

export default MyHomeNavigator;