import { createStackNavigator } from "@react-navigation/stack"
import BackBar from "app/components/BackBar";
import ProfileBar from "app/components/ProfileBar";
import ProfileScreen from "app/screens/Profile";
import SettingScreen from "app/screens/Setting";
import React from "react";

const Stack = createStackNavigator();
const MyAccountStack = createStackNavigator();

const MyAccountNavigator = () => {

    const [animationTypeForReplace, setAnimationTypeForReplace] = React.useState<
        'pop' | 'push' | undefined
    >('push');

    return (
        <MyAccountStack.Navigator
            initialRouteName="ProfileScreen"
            headerMode="screen"
        >
            <Stack.Screen
                name="ProfileScreen"
                component={ProfileScreen}
                options={{
                    animationTypeForReplace,
                    header: props => <ProfileBar />,
                    gestureEnabled: false,
                }}
            />
            <Stack.Screen
                name="SettingScreen"
                component={SettingScreen}
                options={{
                    animationTypeForReplace,
                    header: props => <BackBar />,
                    gestureEnabled: false,
                }}
            />
        </MyAccountStack.Navigator >
    )
}

export default MyAccountNavigator;