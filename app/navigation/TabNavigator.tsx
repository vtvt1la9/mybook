import React, { useState } from 'react';
import { Image, Platform, StyleSheet, Text, View } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { useTheme } from 'react-native-paper';
import GoRiceNavigator from './GoRiceNavigator';
import MyPageNavigator from './InventoryNavigator';
import i18n from '../i18n';
import { useStoreState } from 'easy-peasy';
import { authStateSelector } from '../store';
import MyAccountNavigator from './MyAccountNavigator';
import MyHomeNavigator from './MyHomeNavigator';

const Tab = createBottomTabNavigator();

const TabNavigator = () => {
  const { colors }: any = useTheme();

  const localStyles = React.useMemo(
    () =>
      StyleSheet.create({
        style: {
          backgroundColor: colors.backgroundtabar,
          height: Platform.OS === 'ios' ? 85 : 65,
          paddingTop: Platform.OS === 'ios' ? 10 : 0,
        },
        labelStyle: {
          fontSize: 14,
          fontWeight: 'normal',
        },
        tabStyle: {
          alignSelf: 'center',
          marginVertical: 4,
        },
        wrapperIcon: {
          alignSelf: 'center',
          paddingTop: Platform.OS === 'ios' ? 10 : 0,
        },
        iconTab: {
          width: 30,
          height: 30,
        },
      }),
    [],
  );

  return (
    <Tab.Navigator
      initialRouteName={i18n.t('tab_navigator.go-rice')}
      screenOptions={{
        tabBarStyle: localStyles.style,
        tabBarLabelStyle: localStyles.labelStyle,
        tabBarActiveTintColor: colors.primary,
        tabBarInactiveTintColor: colors.white,
        tabBarActiveBackgroundColor: colors.backgroundtabar,
        tabBarInactiveBackgroundColor: colors.backgroundtabar,
      }}>
      <Tab.Screen
        name={i18n.t('tab_navigator.go-rice')}
        component={MyHomeNavigator}
        options={{
          headerShown: false,
          tabBarLabel: i18n.t('tab_navigator.go-rice'),
          tabBarShowLabel: false,
          tabBarIcon: ({ focused, color, size }) => {
            return (
              <View>
                <View style={localStyles.wrapperIcon}>
                  <Image
                    source={
                      focused
                        ? require('../assets/images/home-active.png')
                        : require('../assets/images/home.png')
                    }
                    style={localStyles.iconTab}
                    resizeMode="contain"
                  />
                </View>
              </View>
            );
          },
        }}
        listeners={{}}
      />
      <Tab.Screen
        name={i18n.t('tab_navigator.inventory')}
        component={MyAccountNavigator}
        options={{
          headerShown: false,
          tabBarLabel: i18n.t('tab_navigator.inventory'),
          tabBarShowLabel: false,
          tabBarIcon: ({ focused, color, size }) => {
            return (
              <View>
                <View style={localStyles.wrapperIcon}>
                  <Image
                    source={
                      focused
                        ? require('../assets/images/my-account-active.png')
                        : require('../assets/images//my-account.png')
                    }
                    style={localStyles.iconTab}
                    resizeMode="contain"
                  />
                </View>
              </View>
            );
          },
        }}
        listeners={{}}
      />
    </Tab.Navigator>
  );
};

export default TabNavigator;
