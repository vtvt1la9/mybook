import {useEffect, useCallback, useMemo, useState} from 'react';
import {useStoreState, useStoreActions} from 'easy-peasy';
import Geolocation, {
  GeolocationResponse,
} from 'react-native-geolocation-service';
import RNLocalize from 'react-native-localize';
import momentTz from 'moment-timezone';

import {
  homeStateSelector,
  modalActionSelector,
  homeActionSelector,
} from 'app/store';
import {getCaloriesBySpeed, getMiningByCalories} from 'app/utils/generator';
import log from 'app/utils/sentry';
import {cycleHistory} from 'app/service/firestore';
import {DEFAULT_POSITION} from 'app/config/constant-config';

export const useGeolocation = () => {
  const {riceStart, formulas, riceHistory} = useStoreState(homeStateSelector);
  const {setIsStopMiningVisible} = useStoreActions(modalActionSelector);
  const {setRegion, getCollectionId, setRiceHistory} =
    useStoreActions(homeActionSelector);
  const [position, setPosition] = useState<GeolocationResponse>();

  // get collection id
  const collectionId: null | string = useMemo(() => {
    if (!riceStart?.firestore_collection?.document_id) {
      return null;
    }

    return riceStart?.firestore_collection?.document_id;
  }, [riceStart]);

  // calculate calories
  const getCalories = useCallback(
    (averageSpeed: number, time: number) => {
      console.log('getCalories', averageSpeed, time);
      if (!formulas.calories) {
        return 0;
      }

      return (
        formulas.calories.data.weight *
        formulas.calories.data.bike_weight *
        getCaloriesBySpeed(averageSpeed) *
        time
      );
    },
    [formulas],
  );

  // calculate mining
  const getMining = useCallback(
    (calories: number) => {
      if (!formulas.mining) {
        return 0;
      }

      return (
        (formulas.mining.data.highest_durability -
          (100 - formulas.mining.data.product_inventory_durability) *
            formulas.mining.data.ratio_of_durability) *
        (formulas.mining.data.value_category + formulas.mining.data.level) *
        getMiningByCalories(calories)
      );
    },
    [formulas],
  );

  useEffect(() => {
    // check if collection id exists, sync with firestore
    log.debug('collectionId', collectionId);
    if (collectionId) {
      log.debug('startSubcribe');
      const watchid = Geolocation.watchPosition(
        pos => {
          log.debug('pos', JSON.stringify(pos));
          if (
            pos.coords.speed &&
            pos.coords.speed > 0 &&
            pos.coords.speed <= 40
          ) {
            // Update current lat-lng
            setPosition(pos);
          } else if (pos.coords.speed && pos.coords.speed > 40) {
            setIsStopMiningVisible(true);
          }
        },
        error => log.debug('WatchPosition Error', JSON.stringify(error)),
        {
          distanceFilter: 5,
          enableHighAccuracy: true,
        },
      );

      const subscriber = cycleHistory
        .doc(collectionId)
        .onSnapshot(documentSnapshot => {
          if (documentSnapshot) {
            const data = documentSnapshot.data();
            if (data) {
              setRiceHistory(data);
            }
          }
        });
      return () => {
        log.debug('unsubribe');
        subscriber();
        Geolocation.clearWatch(watchid);
      };
    }
  }, [collectionId]);

  useEffect(() => {
    log.debug('start use Geolocation');
    getCollectionId()
      .then(res => log.debug('getCollectionId', JSON.stringify(res)))
      .catch(error => {
        log.debug('getCollectionId error:', error);
        Geolocation.getCurrentPosition(
          pos => {
            log.debug('getCurrentPosition', JSON.stringify(pos));
            setRegion({
              latitude: pos?.coords?.latitude,
              longitude: pos?.coords?.longitude,
            });
          },
          errorGeo => {
            log.debug('GetCurrentPosition Error', errorGeo);
            setRegion(DEFAULT_POSITION);
          },
          {
            enableHighAccuracy: true,
            forceRequestLocation: true,
          },
        );
      });
  }, []);

  useEffect(() => {
    if (collectionId && position?.coords) {
      // get current time
      const currentTimeFormat = momentTz
        .tz(new Date(), RNLocalize.getTimeZone())
        .clone()
        .tz('Asia/Seoul')
        .format('YYYY-MM-DD HH:mm:ss');

      // get duration by seconds
      const duration = momentTz(currentTimeFormat, 'YYYY-MM-DD HH:mm:ss').diff(
        momentTz(riceHistory.created_at, 'YYYY-MM-DD HH:mm:ss'),
        'seconds',
      );

      // get calories & mining
      const calories =
        riceHistory.calories +
        getCalories(position.coords.speed || 0, duration); // calculate calories
      const mining = riceHistory.mining + getMining(calories); // calculate mining

      // update firestore
      cycleHistory
        .doc(collectionId)
        .update({
          address_from: `{"latitude":${position.coords.latitude},"longitude":${position.coords.longitude}}`,
          address_from_json: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          },
          'content_json.mining': riceHistory.mining + mining,
          average_speed: position.coords.speed,
          distance: riceHistory.distance + 0.05,
          mining,
          calories,
          content: `{"mining":${
            riceHistory.mining + mining
          },"goal":${JSON.stringify(riceHistory.content_json.goal)}}`,
        })
        .then(() => {
          console.log('User updated!');
        })
        .catch(error => console.log('update firestore error:', error));
    }
  }, [position]);

  return {
    collectionId,
    position,
  };
};
