import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Text,
  Platform,
  Dimensions,
} from 'react-native';
import { useTheme } from 'react-native-paper';
import NavigationService from 'app/navigation/NavigationService';
import { useStoreState } from 'easy-peasy';
import { authStateSelector } from '../store';
import { _abbreviateNumber, formatMoneyTow } from 'app/utils/formatString';
import styles from 'app/styles';
import FastImage from 'react-native-fast-image';

const HomeBar: React.FC = () => {
  const { colors }: any = useTheme();
  const { user, wallets } = useStoreState(authStateSelector);
  const [walletsState, setWalletsState] = useState<any[]>([]);

  useEffect(() => {
    if (wallets[0] && wallets[1]) {
      setWalletsState([wallets[0], wallets[1]]);
    }
  }, [wallets[0]?.amount, wallets[1]?.amount]);

  const localStyles = React.useMemo(
    () =>
      StyleSheet.create({
        header: {
          flexDirection: 'row',
          backgroundColor: colors.white,
          paddingHorizontal: 16,
          paddingTop: Platform.OS === 'android' ? 12 : 50,
          paddingBottom: 12,
          justifyContent: 'space-between',
        },
        tinyLogo: {
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
        },
        textLogo: {
          fontSize: 12,
          fontWeight: 'bold',
          marginRight: 10,
        },
        layout: {
          color: colors.text_content,
          borderColor: colors.gold_500,
          borderRadius: 10,
          backgroundColor: colors.gold_500,
          flexDirection: 'row',
          flexWrap: 'wrap',
          alignItems: 'center',
          paddingVertical: 4,
          paddingHorizontal: 10,
        },
        iconCoin: {
          width: 22,
          height: 22,
        },
        iconAvatar: {
          width: 40,
          height: 40,
          borderRadius: 50,
        },
        iconDoText: {
          color: colors.white,
          fontSize: 12,
          fontWeight: 'bold',
          lineHeight: 15,
          marginHorizontal: 5,
        },
      }),
    [],
  );

  const _redirectToProfileScreen = () => {
    NavigationService.navigate('ProfileScreen');
  };

  return (
    <View style={[localStyles.header]}>
      <TouchableOpacity onPress={_redirectToProfileScreen}>
        <View style={localStyles.tinyLogo}>
          <Image
            source={{ uri: user?.avatar || user?.uri }}
            style={localStyles.iconAvatar}
          />
        </View>
      </TouchableOpacity>
      <View
        style={[
          styles.row,
          {
            justifyContent: 'space-between',
            alignItems: 'center',
            width: Dimensions.get('window').width - 82,
          },
        ]}>
        <TouchableOpacity onPress={_redirectToProfileScreen}>
          <Text style={localStyles.textLogo}>{user?.full_name}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={_redirectToProfileScreen}
          >
          <FastImage
            source={require('../assets/images/bar-icon.png')}
            style={{ width: 30, height: 30 }}
            resizeMode={FastImage.resizeMode.contain}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default HomeBar;
