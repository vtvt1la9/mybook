import React from 'react';
import {
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Text,
    Platform,
} from 'react-native';
import { useTheme } from 'react-native-paper';
import { useRoute } from '@react-navigation/native';

import i18n from 'app/i18n';
import styles from 'app/styles';
import NavigationService from 'app/navigation/NavigationService';
import FastImage from 'react-native-fast-image';

const BookBar: React.FC = () => {
    const { colors }: any = useTheme();
    const route = useRoute();

    const localStyles = React.useMemo(
        () =>
            StyleSheet.create({
                header: {
                    backgroundColor: colors.white,
                    paddingHorizontal: 16,
                    paddingTop: Platform.OS === 'android' ? 18 : 50,
                    paddingBottom: 12,
                    justifyContent: 'center'
                },
                headerContainer: {
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between'
                },
                iconBack: {
                    width: 8.43,
                    height: 16,
                },
                iconOptionContainer: {
                    flexDirection: 'row',
                    alignItems: 'center'
                },
                iconOption: {
                    width: 16,
                    height: 16,
                    marginHorizontal: 5
                }
            }),
        [],
    );

    const _goBack = () => {
        NavigationService.goBack();
    };

    return (
        <View style={[localStyles.header, localStyles.headerContainer]}>
            <TouchableOpacity onPress={_goBack}>
                <FastImage
                    source={require('../assets/images/icon-back.png')}
                    resizeMode={FastImage.resizeMode.contain}
                    style={localStyles.iconBack}
                />
            </TouchableOpacity>
            <View style={[localStyles.iconOptionContainer]}>
                <TouchableOpacity>
                    <FastImage
                        source={require('../assets/images/heart.png')}
                        resizeMode={FastImage.resizeMode.contain}
                        style={localStyles.iconOption}
                    />
                </TouchableOpacity>
                <TouchableOpacity>
                    <FastImage
                        source={require('../assets/images/star.png')}
                        resizeMode={FastImage.resizeMode.contain}
                        style={localStyles.iconOption}
                    />
                </TouchableOpacity>
                <TouchableOpacity>
                    <FastImage
                        source={require('../assets/images/share.png')}
                        resizeMode={FastImage.resizeMode.contain}
                        style={localStyles.iconOption}
                    />
                </TouchableOpacity>
            </View>
        </View>
    );
};

export default BookBar;
