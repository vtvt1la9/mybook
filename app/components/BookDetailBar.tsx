import React from 'react';
import {
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Text,
    Platform,
} from 'react-native';
import { useTheme } from 'react-native-paper';
import { useRoute } from '@react-navigation/native';

import i18n from 'app/i18n';
import styles from 'app/styles';
import NavigationService from 'app/navigation/NavigationService';
import FastImage from 'react-native-fast-image';

const BookDetailBar: React.FC = () => {
    const { colors }: any = useTheme();
    const route = useRoute();

    const localStyles = React.useMemo(
        () =>
            StyleSheet.create({
                header: {
                    backgroundColor: colors.white,
                    paddingHorizontal: 16,
                    paddingTop: Platform.OS === 'android' ? 18 : 50,
                    paddingBottom: 12,
                },
                headerContainer: {
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-around'
                },
                iconBack: {
                    width: 8.43,
                    height: 16,
                },
                textName: {
                    fontSize: 20,
                    fontWeight: '600'
                }

            }),
        [],
    );

    const _goBack = () => {
        NavigationService.goBack();
    };

    return (
        <View style={[localStyles.header, localStyles.headerContainer]}>
            <TouchableOpacity style={{ width: '40%' }} onPress={_goBack}>
                <FastImage
                    source={require('../assets/images/icon-back.png')}
                    resizeMode={FastImage.resizeMode.contain}
                    style={localStyles.iconBack}
                />
            </TouchableOpacity>
            <View style={{ width: '60%' }}>
                <Text style={[localStyles.textName]}>The Aviora</Text>
            </View>
        </View>
    );
};

export default BookDetailBar;
