import { useNavigation } from "@react-navigation/native";
import { useTheme } from "react-native-paper";
import i18n from "app/i18n";
import React from "react";
import { Text, View, StyleSheet, ScrollView, TouchableOpacity } from "react-native"
import FastImage from "react-native-fast-image";

const HomeBookScreen = () => {
    const { colors }: any = useTheme();
    const navigation = useNavigation();

    const localStyles = React.useMemo(() => StyleSheet.create({
        container: {
            flex: 1,
            paddingHorizontal: 16,
            backgroundColor: colors.background,
        },
        imageContainer: {
            alignItems: 'center',
            justifyContent: 'center'
        },
        image: {
            width: 250,
            height: 300
        },
        starIconContainer: {
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            marginVertical: 10
        },
        starIcon: {
            width: 20,
            height: 20,
            marginHorizontal: 5
        },
        textContainer: {
            alignItems: 'center'
        },
        textName: {
            fontSize: 36,
            fontWeight: '800',
        },
        textAuthor: {
            fontSize: 20,
        },
        textDescription: {
            fontSize: 16,
            textAlign: 'center',
            marginTop: 10
        },
        buttonContainer: {
            width: 200,
            backgroundColor: colors.green,
            borderRadius: 5,
            flexDirection: 'row',
            alignItems: 'center',
        },
        textButton: {
            color: colors.black,
            paddingVertical: 10,
            paddingLeft: 30,
            fontSize: 16,
            fontWeight: '600'
        }
    }), [])

    const _goToReadBookScreen = () => {
        navigation.navigate('ReadBookScreen');
    }

    return (
        <ScrollView style={[localStyles.container]}>
            <View style={[localStyles.imageContainer]}>
                <FastImage
                    source={require('../../assets/images/manga.png')}
                    style={[localStyles.image]}
                    resizeMode={FastImage.resizeMode.cover}
                />
            </View>
            <View style={[localStyles.starIconContainer]}>
                <FastImage
                    source={require('../../assets/images/star.png')}
                    style={[localStyles.starIcon]}
                    resizeMode={FastImage.resizeMode.contain}
                />
                <FastImage
                    source={require('../../assets/images/star.png')}
                    style={[localStyles.starIcon]}
                    resizeMode={FastImage.resizeMode.contain}
                />
                <FastImage
                    source={require('../../assets/images/star.png')}
                    style={[localStyles.starIcon]}
                    resizeMode={FastImage.resizeMode.contain}
                />
                <FastImage
                    source={require('../../assets/images/star.png')}
                    style={[localStyles.starIcon]}
                    resizeMode={FastImage.resizeMode.contain}
                />
                <FastImage
                    source={require('../../assets/images/star.png')}
                    style={[localStyles.starIcon]}
                    resizeMode={FastImage.resizeMode.contain}
                />
            </View>
            <View style={[localStyles.textContainer]}>
                <Text style={[localStyles.textName]}>The Arsonist</Text>
                <Text style={[localStyles.textAuthor]}>Van Thin</Text>
                <Text style={[localStyles.textDescription]}>Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam, aperiam, blanditiis praesentium quia, eos suscipit qui aliquid culpa nesciunt quaerat nihil quas voluptatem quasi! Temporibus aperiam natus corporis in corrupti.</Text>
            </View>
            <View style={{ alignItems: 'flex-end', marginTop: 20 }}>
                <TouchableOpacity onPress={_goToReadBookScreen} style={[localStyles.buttonContainer]}>
                    <Text style={[localStyles.textButton]}>{i18n.t('home.read_book')}</Text>
                    <FastImage
                        source={require('../../assets/images/right-arrow-icon.png')}
                        style={[localStyles.starIcon]}
                        resizeMode={FastImage.resizeMode.contain}
                    />
                </TouchableOpacity>
            </View>
        </ScrollView>
    )
}

export default HomeBookScreen;