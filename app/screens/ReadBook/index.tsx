import { useNavigation } from "@react-navigation/native";
import React, { useState } from "react";
import { ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native"
import FastImage from "react-native-fast-image";
import { useTheme } from "react-native-paper";

const ReadBookScreen = () => {

    const { colors }: any = useTheme();
    const navigation = useNavigation();
    const [isDark, setIsDark] = useState(false);
    const [isUnderLine, setIsUnderLine] = useState(false);

    const localStyles = React.useMemo(() => StyleSheet.create({
        container: {
            flex: 1,
            paddingHorizontal: 16,
        },
        textChap: {
            fontSize: 16,
            fontWeight: '600',
            textAlign: 'center'
        },
        textDescription: {
            fontSize: 16,
            textTransform: 'capitalize',
            textAlign: 'justify',
            lineHeight: 20
        },
        iconOptionsContainer: {
            flexDirection: 'row',
            justifyContent: 'space-around',
            width: '80%',
            alignItems: 'center',
            alignSelf: 'center',
            paddingVertical: 20
        },
        iconContainer: {
            padding: 5,
            backgroundColor: colors.background
        }
    }), [])

    const _darkBackground = () => {
        setIsDark(!isDark)
    }

    const _underLineText = () => {
        setIsUnderLine(!isUnderLine)
    }

    return (
        <View style={[localStyles.container, { backgroundColor: !isDark ? colors.background : colors.black }]}>
            <ScrollView
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
            >
                <Text style={[localStyles.textChap]}>Chap 1</Text>
                <View style={{ marginTop: 10 }}>
                    <Text style={[localStyles.textDescription, { color: !isDark ? colors.black : colors.white, textDecorationLine: isUnderLine ? 'underline' : 'none' }]}>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt iusto veniam at similique suscipit dicta deserunt porro corporis laudantium non, eum nihil harum provident. Velit harum ipsum fuga suscipit quos.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt iusto veniam at similique suscipit dicta deserunt porro corporis laudantium non, eum nihil harum provident. Velit harum ipsum fuga suscipit quos.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt iusto veniam at similique suscipit dicta deserunt porro corporis laudantium non, eum nihil harum provident. Velit harum ipsum fuga suscipit quos.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt iusto veniam at similique suscipit dicta deserunt porro corporis laudantium non, eum nihil harum provident. Velit harum ipsum fuga suscipit quos.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt iusto veniam at similique suscipit dicta deserunt porro corporis laudantium non, eum nihil harum provident. Velit harum ipsum fuga suscipit quos.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt iusto veniam at similique suscipit dicta deserunt porro corporis laudantium non, eum nihil harum provident. Velit harum ipsum fuga suscipit quos.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt iusto veniam at similique suscipit dicta deserunt porro corporis laudantium non, eum nihil harum provident. Velit harum ipsum fuga suscipit quos.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt iusto veniam at similique suscipit dicta deserunt porro corporis laudantium non, eum nihil harum provident. Velit harum ipsum fuga suscipit quos.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt iusto veniam at similique suscipit dicta deserunt porro corporis laudantium non, eum nihil harum provident. Velit harum ipsum fuga suscipit quos.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt iusto veniam at similique suscipit dicta deserunt porro corporis laudantium non, eum nihil harum provident. Velit harum ipsum fuga suscipit quos.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt iusto veniam at similique suscipit dicta deserunt porro corporis laudantium non, eum nihil harum provident. Velit harum ipsum fuga suscipit quos.Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt iusto veniam at similique suscipit dicta deserunt porro corporis laudantium non, eum nihil harum provident. Velit harum ipsum fuga suscipit quos.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt iusto veniam at similique suscipit dicta deserunt porro corporis laudantium non, eum nihil harum provident. Velit harum ipsum fuga suscipit quos.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt iusto veniam at similique suscipit dicta deserunt porro corporis laudantium non, eum nihil harum provident. Velit harum ipsum fuga suscipit quos.
                    </Text>
                </View>
            </ScrollView>
            <View style={[localStyles.iconOptionsContainer]}>
                <TouchableOpacity onPress={_underLineText} style={[localStyles.iconContainer]}>
                    <FastImage
                        source={require('../../assets/images/text-underline.png')}
                        resizeMode={FastImage.resizeMode.contain}
                        style={{ width: 20, height: 20 }}
                    />
                </TouchableOpacity>
                <TouchableOpacity style={[localStyles.iconContainer]}>
                    <FastImage
                        source={require('../../assets/images/dot-bar.png')}
                        resizeMode={FastImage.resizeMode.contain}
                        style={{ width: 20, height: 20 }}
                    />
                </TouchableOpacity>
                <TouchableOpacity style={[localStyles.iconContainer]} onPress={_darkBackground}>
                    <FastImage
                        source={!isDark ? require('../../assets/images/moon-icon.png') : require('../../assets/images/sun-icon.png')}
                        resizeMode={FastImage.resizeMode.contain}
                        style={{ width: 20, height: 20 }}
                    />
                </TouchableOpacity>
                <TouchableOpacity style={[localStyles.iconContainer]}>
                    <FastImage
                        source={require('../../assets/images/head-phone-icon.png')}
                        resizeMode={FastImage.resizeMode.contain}
                        style={{ width: 20, height: 20 }}
                    />
                </TouchableOpacity>
                <TouchableOpacity style={[localStyles.iconContainer]}>
                    <FastImage
                        source={require('../../assets/images/pencil-icon.png')}
                        resizeMode={FastImage.resizeMode.contain}
                        style={{ width: 20, height: 20 }}
                    />
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default ReadBookScreen;