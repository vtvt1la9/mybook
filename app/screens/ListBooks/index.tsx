import { Text, View, StyleSheet, TouchableOpacity, FlatList, Dimensions } from "react-native"
import { listReadingContinue } from "../MyHome/database/listReadingContinue";
import React from "react";
import { useTheme } from "react-native-paper";
import FastImage from "react-native-fast-image";
import { useNavigation } from "@react-navigation/native";

const ListBookScreen = () => {

    const { colors } = useTheme();
    const navigation = useNavigation();

    const localStyles = React.useMemo(
        () =>
            StyleSheet.create({
                container: {
                    flexGrow: 1,
                    paddingHorizontal: 16,
                    backgroundColor: colors.background,
                    alignItems: 'center'
                },
                flatlistContainer: {
                    marginVertical: 10,
                },
                flatListContainer: {
                    marginHorizontal: 5,
                    marginBottom: 20
                },
                textFlatlist: {
                    textAlign: 'center',
                    color: colors.red,
                    fontSize: 16
                },
                listReadingContinueContainer: {
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginBottom: 5,
                    width: Dimensions.get('window').width / 2 - 32,
                    height: 200,
                },
                readingContinueImageContainer: {
                    width: '100%',
                    height: '100%',
                    borderRadius: 20
                },
            }),
        [],
    );

    const _renderListReadingContinue = (item: any, index: any) => {
        return (
            <>
                <TouchableOpacity onPress={_goToHomeBookScreen} style={[localStyles.flatListContainer]}>
                    <View style={[localStyles.listReadingContinueContainer]}>
                        <FastImage
                            source={require('../../assets/images/manga.png')}
                            style={[localStyles.readingContinueImageContainer]}
                            resizeMode={FastImage.resizeMode.cover}
                        />
                    </View>
                    <Text style={[localStyles.textFlatlist]}>{item.author}</Text>
                </TouchableOpacity>
            </>
        )
    }

    const _goToHomeBookScreen = () => {
        navigation.navigate('HomeBookScreen');
    }

    return (
        <View style={[localStyles.container]}>
            <FlatList
                data={listReadingContinue}
                numColumns={2}
                renderItem={({ item, index }: any) => _renderListReadingContinue(item, index)}
                keyExtractor={item => JSON.stringify(item).toString()}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
            />
        </View>
    )
}

export default ListBookScreen;