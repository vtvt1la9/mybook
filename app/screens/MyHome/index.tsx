import {
    View,
    StyleSheet,
    StatusBar,
    Text,
    Image,
    Dimensions,
    TouchableOpacity,
    Platform,
    FlatList
  } from 'react-native';
  import React, { useState, useEffect, useMemo } from 'react';
  import { Button, useTheme } from 'react-native-paper';
  import Orientation from 'react-native-orientation';
  import { useStoreActions, useStoreState } from 'easy-peasy';
  import {
    authActionSelector,
    authStateSelector,
    homeActionSelector,
    homeStateSelector,
    modalActionSelector,
    modalStateSelector,
    shopStateSelector,
  } from 'app/store';
  import { useNavigation } from '@react-navigation/native';
  import i18n from 'app/i18n';
  import styles from 'app/styles';
  import NavigationService from 'app/navigation/NavigationService';
  import { ScrollView } from 'react-native-gesture-handler';
  import FastImage from 'react-native-fast-image';
  import { listAuthors } from './database/listAuthors';
  import { listReadingContinue } from './database/listReadingContinue';
  
  const MyHomeScreen = () => {
    const { colors }: any = useTheme();
    const navigation = useNavigation();
  
    const localStyles = React.useMemo(
      () =>
        StyleSheet.create({
          container: {
            flexGrow: 1,
            paddingHorizontal: 16,
            backgroundColor: colors.background,
          },
          flatlistContainer: {
            marginVertical: 10
          },
          textTitleContainer: {
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginVertical: 10,
            alignItems: 'center'
          },
          textTitle: {
            fontSize: 20,
            fontWeight: '600'
          },
          flatListContainer: {
            marginHorizontal: 10
          },
          authorImageContainer: {
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 5
          },
          authorImage: {
            width: 50,
            height: 50,
            borderRadius: 50,
            borderWidth: 2,
            borderColor: colors.red
          },
          textFlatlist: {
            textAlign: 'center',
            color: colors.red,
            fontSize: 16
          },
          listReadingContinueContainer: {
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 5
          },
          readingContinueImageContainer: {
            width: 120,
            height: 150,
            borderRadius: 20
          },
          textTouchAbleOpacity: {
            fontSize: 14,
            color: colors.black
          }
        }),
      [],
    );
  
    const _goToHomeBookScreen = () => {
      navigation.navigate('HomeBookScreen');
    }
  
    const _goToListBookScreen = () => {
      navigation.navigate('ListBookScreen');
    }
  
    const _renderListAuthors = (item: any, index: any) => {
      return (
        <>
          <TouchableOpacity style={[localStyles.flatListContainer]}>
            <View style={[localStyles.authorImageContainer]}>
              <FastImage
                source={require('../../assets/images/icon-check-path.png')}
                style={[localStyles.authorImage]}
                resizeMode={FastImage.resizeMode.contain}
              />
            </View>
            <Text style={[localStyles.textFlatlist]}>{item.author}</Text>
          </TouchableOpacity>
        </>
  
      )
    }
  
    const _renderListReadingContinue = (item: any, index: any) => {
      return (
        <>
          <TouchableOpacity onPress={_goToHomeBookScreen} style={[localStyles.flatListContainer]}>
            <View style={[localStyles.listReadingContinueContainer]}>
              <FastImage
                source={require('../../assets/images/manga.png')}
                style={[localStyles.readingContinueImageContainer]}
                resizeMode={FastImage.resizeMode.cover}
              />
            </View>
            <Text style={[localStyles.textFlatlist]}>{item.author}</Text>
          </TouchableOpacity>
        </>
      )
    }
  
    const _renderListTrendingBook = (item: any, index: any) => {
      return (
        <>
          <TouchableOpacity onPress={_goToHomeBookScreen} style={[localStyles.flatListContainer]}>
            <View style={[localStyles.listReadingContinueContainer]}>
              <FastImage
                source={require('../../assets/images/manga.png')}
                style={[localStyles.readingContinueImageContainer]}
                resizeMode={FastImage.resizeMode.cover}
              />
            </View>
            <Text style={[localStyles.textFlatlist]}>{item.author}</Text>
          </TouchableOpacity>
        </>
  
      )
    }
  
    return (
      <ScrollView
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        style={localStyles.container}>
        <StatusBar
          barStyle={'dark-content'}
          animated={true}
          backgroundColor={colors.splash}
        />
        {/* List Author */}
        <View style={[localStyles.flatlistContainer]}>
          <View style={[localStyles.textTitleContainer]}>
            <Text style={[localStyles.textTitle]}>{i18n.t('home.popular_authors')}</Text>
            <TouchableOpacity onPress={_goToListBookScreen}>
              <Text style={[localStyles.textTouchAbleOpacity]}>{i18n.t('home.view_all')}</Text>
            </TouchableOpacity>
          </View>
          <FlatList
            data={listAuthors}
            renderItem={({ item, index }: any) => _renderListAuthors(item, index)}
            horizontal={true}
            keyExtractor={item => JSON.stringify(item).toString()}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
          // onEndReached={_handleLoadMore}
          // onEndReachedThreshold={0.5}
          // refreshControl={
          //   <RefreshControl
          //     title={i18n.t('pull_to_refresh')}
          //     tintColor={colors.primary}
          //     titleColor={colors.primary}
          //     colors={[colors.primary, 'green', 'blue']}
          //     refreshing={isRefreshing}
          //     onRefresh={_handleRefresh}
          //   />
          // }
          // ListEmptyComponent={() => _renderItemEmpty}
          />
        </View>
        {/* List Author */}
        {/* List reading continue */}
        <View style={[localStyles.flatlistContainer]}>
          <View style={[localStyles.textTitleContainer]}>
            <Text style={[localStyles.textTitle]}>{i18n.t('home.continue_reading')}</Text>
            <TouchableOpacity onPress={_goToListBookScreen}>
              <Text style={[localStyles.textTouchAbleOpacity]}>{i18n.t('home.view_all')}</Text>
            </TouchableOpacity>
          </View>
          <FlatList
            data={listReadingContinue}
            renderItem={({ item, index }: any) => _renderListReadingContinue(item, index)}
            horizontal={true}
            keyExtractor={item => JSON.stringify(item).toString()}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
          />
        </View>
        {/* List reading continue */}
        {/* Trending Books */}
        <View style={[localStyles.flatlistContainer]}>
          <View style={[localStyles.textTitleContainer]}>
            <Text style={[localStyles.textTitle]}>{i18n.t('home.trending_book')}</Text>
            <TouchableOpacity onPress={_goToListBookScreen}>
              <Text style={[localStyles.textTouchAbleOpacity]}>{i18n.t('home.view_all')}</Text>
            </TouchableOpacity>
          </View>
          <FlatList
            data={listReadingContinue}
            renderItem={({ item, index }: any) => _renderListTrendingBook(item, index)}
            horizontal={true}
            keyExtractor={item => JSON.stringify(item).toString()}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
          />
        </View>
        {/* Trending Books */}
      </ScrollView>
    );
  };
  
  export default MyHomeScreen;
  